const express = require("express");
const cors = require("cors");
const path = require("path");

require("dotenv").config();

const db = require("./app/models");

const {initial} = require("./data");

// Khởi tạo Express App
const app = express();

app.use(cors());

app.use(express.json());

const PORT = process.env.ENV_PORT || 8000;

app.use(express.static(__dirname + "/app/views"))

db.mongoose
    .connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
    .then(() => {
        console.log("Connect mongoDB Successfully");
        initial();
    })
    .catch((err) => {
        console.error("Connection error", err);
        process.exit();
    })

app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/login.html"))
})

app.get("/signup", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/signup.html"))
})

app.use("/api/auth/", require("./app/routes/auth.route"));

app.listen(PORT, () => {
    console.log(`App Listening on port ${PORT}`);
})